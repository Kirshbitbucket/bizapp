##############################################################
# Variables
##############################################################
#variable "private_key_path" {
#  type = string
#  default = "~./terraform.pem"
#}
variable "AWS_ACCESS_KEY" {
}

variable "AWS_SECRET_KEY" {
}
##############################################################
# Provider Details
##############################################################

provider "aws" {
  region = "ap-south-1"
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}

################################################################
# Resource - Security Groups
################################################################

#resource "aws_security_group" "allow_all" {
#  name = "allow_all"
#  description = "allow all the inbound traffic"

  #ingress {
#    from_port = 0
#    to_port = 65535
#    protocol = "tcp"
#    cidr_blocks = ["0.0.0.0/1"]
#  }
#}

################################################################
# Resource - Instances
################################################################

resource "aws_instance" "tfmtestinstance" {
  ami             = data.aws_ami.ami.id
  instance_type   = "t2.micro"
  security_groups = ["allow_all"]
  key_name = "terraform"
}
#################################################################
# Output
#################################################################

output "public_ip" {
  value = "${aws_instance.tfmtestinstance.public_ip}"
}
