data "aws_ami" "ami" {

  filter {
    name = "name"
    values = ["packer-example *"]
  }

  most_recent = true
  owners = ["823625746156"]

}
