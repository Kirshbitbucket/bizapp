#!/bin/sh

cd ~/installs
curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.22.tgz
tar -xvf mongodb-linux-x86_64-3.2.22.tgz
mv mongodb-linux-x86_64-3.2.22.tgz mongodb

#############################################################3
## Create directory for mongo data at ~/db/mongo/data
############################################################

cd /
mkdir db
cd db
mkdir mongo
cd mongo
mkdir data
cd data

##################################################################
###  Create mongodbconfig
#################################################################
cd ~/installs/mongodb/bin
vim mongodbconfig

# Paste the below comments
dbpath = /db/mongo/data
logpath = /db/mongo/mongodb.log
logappend = true
port = 27017
fork = true
#replSet = abc

###########################################################3
### Create mongostart
###########################################################3
cd ~/installs/mongodb/bin
vim mongostart

## Paste the below comments
sudo ~/installs/mongodb/bin/mongod --config ~/installs/mongodb/bin/mongodbconfig


###########################################################3
### Create mongostop
###########################################################3
cd ~/installs/mongodb/bin
vim mongostop

## Paste the below comments
#!/bin/bash
pid=`ps -aef | grep mongod | grep config | tr -s " " | cut -f3 -d " "`;
if [ "${pid}" != "" ]; then
sudo kill -2 ${pid};
fi
