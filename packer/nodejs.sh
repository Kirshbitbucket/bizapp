###################################################
## Installation of Nodejs & npm
###################################################
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
sudo apt-get install -y nodejs

###################################################
## Installation of gulp
###################################################
npm install -g gulp
npm install --save-dev gulp-install
