# download android sdk
sudo curl https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip -o android-sdk.zip

# install unzip command
sudo apt-get install unzip

# unzip into this folder
sudo unzip android-sdk.zip -d .

# remove the zip file
sudo rm android-sdk.zip
